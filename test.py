'''This module tests a bunch of policy definitions against supplied testcases'''
from os import environ, getenv
import sys
import logging
import subprocess
from json import load as j_load
from json import loads as j_loads
from json import dumps as j_dumps
from yaml import safe_load as yaml_safe_load
from yaml import safe_load_all as yaml_safe_load_all
from dotenv import load_dotenv
import requests
import tabulate
import urllib3

# default configuration
# by using dotenv we can specify the environment in ENV or in .env file
load_dotenv()
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

# helper functions
def str2bool(v):
    '''takes a string value and returns a boolean value'''
    return str(v).lower() in ("yes", "true", "1")

def load_polfile(p_filepath):
    '''loads a policy_file and returns it as json'''
    with open(p_filepath, "r", encoding='utf8') as pol_file:
        return j_load(pol_file)

def get_stackrox_policies():
    '''reads all policies from a stackrox api endpoint and returns them as json'''
    # Only for development, might not be needed in the end
    api_url = "https://" + ROX_CENTRAL_ADDRESS + '/v1/policies'
    headers = {"Authorization": f"Bearer {ROX_API_TOKEN}"}
    try:
        response = requests.get(api_url, headers=headers,
                                verify=API_VERIFY_SSL, timeout=REQUESTS_TIMEOUT)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        raise SystemExit(e) from e

    return response.json()

def ensure_policy(pol_filepath):
    '''reads a policy from the filesystem and imports it in stackrox using the import API'''
    # Function ensures, that a policy is existing in stackrox
    # we expect global vars for ROX_CENTRAL_ADDRESS, ROX_API_TOKEN and ROX_VERIFY_SSL
    # delete policy to be sure the tested policy is the one in the system
    headers = {"Authorization": f"Bearer {ROX_API_TOKEN}"}
    policy_spec = load_polfile(pol_filepath)

    # Import Policy to ensure its existence as defined
    import_api_url = "https://" + ROX_CENTRAL_ADDRESS + '/v1/policies/import'
    # the import api works with metadata overwrite: true, so we need to construct a new dict
    # this also protects us from needing to delete the policies beforehand
    import_json = {"metadata": {"overwrite": True}, "policies": policy_spec['policies']}
    try:
        import_response = requests.post(import_api_url, headers=headers,
                                        verify=API_VERIFY_SSL, json=import_json,
                                        timeout=REQUESTS_TIMEOUT)
    except requests.exceptions.RequestException as e:
        raise SystemExit(e) from e

    if import_response.status_code == 200:
        logging.debug("Imported Policies")
        return True

    logging.error("Failed to import Policy")
    return False

def check_policy_metadata(pol_filepath):
    '''Function ensures, that metadata is configured as expected'''
    content = load_polfile(pol_filepath)
    metadata_checkresult = True
    # We expect every policy to be in the BSI Category
    # a file can include multiple policies
    for pol in content["policies"]:
        if not "BSI" in pol["categories"]:
            logging.error("Metadata check did not find BSI Category on Policy %s", pol_filepath)
            metadata_checkresult = False

        # We expect every policy to be in the defined disabled state
        if not pol["disabled"] == TEST_POLICY_STATE_DISABLED:
            logging.error("Metadata check did find a wrong disabled state %s", pol_filepath)
            metadata_checkresult = False

    return metadata_checkresult

def set_disabled_state_stackrox_policy(uuid, state):
    '''function sets the disabled state for a policy in stackrox'''
    headers = {"Authorization": f"Bearer {ROX_API_TOKEN}"}

    patchpolicy_dict = {'uuid': uuid, 'disabled': state}
    patchpolicy_api_url = "https://" + ROX_CENTRAL_ADDRESS + '/v1/policies/' + uuid
    patchpolicy_response = requests.patch(patchpolicy_api_url, headers=headers,
                                          verify=API_VERIFY_SSL, json=patchpolicy_dict,
                                          timeout=REQUESTS_TIMEOUT)

    if patchpolicy_response.status_code == 200:
        logging.debug("set state %s policy %s", state, uuid)
        return True

    logging.error("Failed to set state %s policy %s", state, uuid)
    return False

def assess_policy(p_filepath,t_filepath):
    '''assesses a policy from a path, ensures it exists and asseses a testdeployment against it'''

    policy = load_polfile(p_filepath)

    # ensure policy exists
    ensure_policy(pol_filepath=p_filepath)
    # enable policy for testcase
    # since there is always just one policy in a json file, it is safe to call the element directly
    set_disabled_state_stackrox_policy(uuid=policy['policies'][0]['id'], state=False )
    # roxctl deployment check --file $TESTFILE --categories BSI -ojson
    # we might be able to further limit this with '--categories', 'BSI', but currently this fails
    if ROX_VERIFY_SSL:
        args = (ROX_BINARY_PATH, 'deployment', 'check', '--file', t_filepath, '-ojson' )
    else:
        args = (ROX_BINARY_PATH, 'deployment', 'check', '--file', t_filepath, '-ojson',
                 '--insecure-skip-tls-verify' )
    with subprocess.Popen(args, stdout=subprocess.PIPE, env=environ.copy()) as popen:
        popen.wait(timeout=ROX_TIMEOUT)
        # get binary output
        # convert to strings
        # convert to dict
        results = j_loads((popen.stdout.read()).decode('utf-8'))

    # check that only BAD policies alert
    # check that ALL BAD testcases alert for the policy
    with open(t_filepath, 'r', encoding='utf8') as testfile:
        # get all names of the deployments
        tests = list(yaml_safe_load_all(testfile))
        good_testnames = [g['metadata']['name'] for g in tests if "good" in g['metadata']['name']]
        bad_testnames = [b['metadata']['name'] for b in tests if "bad" in b['metadata']['name']]

    good_triggers = 0
    bad_triggers = 0
    test_success = False
    if 'results' in results:
        for result in results['results']:
            deployment_name = result['metadata']['additionalInfo']['name']
            violated_policies = [d['name'] for d in result['violatedPolicies']]
            # check that NO GOOD policies alert
            if deployment_name in good_testnames and policy['policies'][0]['name'] in violated_policies:
                logging.error("GOOD TESTCASE %s ALERTS ON POLICY", deployment_name)
                good_triggers += 1
            if deployment_name in bad_testnames and policy['policies'][0]['name'] in violated_policies:
                logging.info("BAD TESTCASE %s TRIGGERS POLICY", deployment_name)
                bad_triggers += 1

        if bad_triggers == len(bad_testnames) and good_triggers == 0:
            logging.info("successfully assessed policy")
            test_success = True
        else:
            logging.error("Error checking policy")
            # sys.exit('Error Checking policy')
    else:
        logging.error("ERROR retrieving results")

    value = {'policyFilePath': p_filepath,
            'testFilePath': t_filepath,
            'expectedGoodTestCases': len(good_testnames),
            'expectedBadTestCaseTriggers': len(bad_testnames),
            'observedBadTestCases': bad_triggers,
            'testSuccess': test_success,
            'metaDataTestSuccess': check_policy_metadata(pol_filepath=p_filepath)
            }
    logging.debug(j_dumps(value, indent=4))
    return value

# Main Code
# Constants
POLICY_DIR = getenv('POLICY_DIR', './policies/')
TEST_DIR = getenv('TEST_DIR', './tests/')
CONFIGFILE = getenv('CONFIGFILE', './policy-spec.yaml')

# stackrox
# needed ENVs
print(environ)
ROX_API_TOKEN = environ['ROX_API_TOKEN']
ROX_CENTRAL_ADDRESS = environ['ROX_CENTRAL_ADDRESS']
# rox_verify_ssl defines the behavior of the roxctl commandline
ROX_VERIFY_SSL = str2bool(getenv('ROX_VERIFY_SSL', str(False)))
REQUESTS_TIMEOUT = int(getenv('REQUESTS_TIMEOUT', str(30)))
ROX_TIMEOUT = int(getenv('ROX_TIMEOUT', str(120)))
ROX_BINARY_PATH = environ['ROX_BINARY_PATH']
# due to difficulties, we currently do not validate for api calls
API_VERIFY_SSL =  getenv('API_VERIFY_SSL', str(True))
if  API_VERIFY_SSL != "True":
    # we disable the warnings, since they clutter our logs.
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
else:
    API_VERIFY_SSL = True
TEST_POLICY_STATE_DISABLED = str2bool(getenv('TEST_POLICY_STATE_DISABLED', str(True)))

logging.getLogger('urllib3').setLevel(logging.ERROR)

def print_json_results(results):
    '''outputs the results as json'''
    print(j_dumps(results, indent=4))

def print_table_results(results):
    '''outputs the results in a human readable table format'''
    tab_header = results[0].keys()
    tab_rows =  [t.values() for t in results]
    print(tabulate.tabulate(tab_rows, tab_header, tablefmt='grid'))

# list of tests with issues
# double policy 013
# no testcases 024, 025, 026
def main():
    '''main function to coordinate the tests'''
    skip_list = ['013', '024', '025', '026']
    test_results = []

    with open(CONFIGFILE, 'r', encoding='utf8') as config:
        policy_spec_yaml = yaml_safe_load(config)

    for item in policy_spec_yaml['policies']:
        if item['stackrox']['implementationType'] != 'none':
            # we load the policyfile to work with the IDs in the policy file.
            # this enables us to ignore differences between manual and upstream policies
            policy_filepath = POLICY_DIR + item['policyID'] + '-' + item['filename'].lower() + '.json'
            policy = load_polfile(policy_filepath)
            test_filepath = TEST_DIR + item['policyID'] + '-' + item['filename'].lower() + '.yaml'
            if item['policyID'] not in skip_list:
                test_results.append(assess_policy(p_filepath=policy_filepath, t_filepath=test_filepath))

            # since there is always just one policy in a json file, it is safe to call the element directly
            set_disabled_state_stackrox_policy(uuid=policy['policies'][0]['id'], state=True )
        else:
            logging.info("not testing policy with policyID %s, since it is not implemented", item["policyID"])

    print_json_results(test_results)
    print_table_results(test_results)

    if not any(r['testSuccess'] is False for r in test_results):
        sys.exit(0)
    sys.exit(1)

if __name__ == "__main__":
    main()
