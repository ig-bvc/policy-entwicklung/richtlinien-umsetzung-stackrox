# this script downloads the default policies from upstream,
# adds IG BvC specific content and adds it to the policy set
'''creates policies as defined in a policy spec'''
import logging
import sys
import os.path
import urllib.request
import json
import yaml

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


POLICY_DIR = './policies/'

def main():
    '''main function'''
# we iterate over every policy in the mirror_config
# download the src
# and modify the parameters
    configfile = './policy-spec.yaml'
    kubelint_conf = { "checks": {"doNotAutoAddDefaults": "true"}, "customChecks" : [] }

    with open(configfile, 'r', encoding='utf8') as config:
        mirror_config = yaml.safe_load(config)
        logging.debug(mirror_config)

    # we set default settings for the policies
    policy_disabled_default = mirror_config["defaults"]["policyDisabled"]

    for item in mirror_config["policies"]:
        if "stackrox" in item:
            if item["stackrox"]["implementationType"] == "none":
                logging.info("processing: %s as none - SKIPPED",item["policyID"])
            elif item["stackrox"]["implementationType"] == "manual":
                logging.info("processing: %s %s as manual",item["policyID"],item["filename"])
                stackrox_manual(mpol=item)
            elif item["stackrox"]["implementationType"] == "upstream":
                logging.info("processing: %s %s as upstream",item["policyID"],item["filename"])
                stackrox_upstream(upol=item, upol_disabled_state=policy_disabled_default)
            else:
                logging.error("did not find an implementationtype for STACKROX implementation")

        # kube-linter implementation
        if "kubelinter" in item:
            if item["kubelinter"]["implementationType"] == "none":
                logging.info("KUBELINT processing: " + item["policyID"] + " as none - SKIPPED")
                continue
            elif item["kubelinter"]["implementationType"] == "template":
                logging.info("KUBELINT processing: " + item["policyID"] + " " + item["filename"] + " as template")
                #kubelint_conf["customChecks"].append(add_kubelinter_templates(kl_item=item))
                kubelint_conf["customChecks"] = kubelint_conf["customChecks"] + add_kubelinter_templates(kl_item=item)
            else:
                logging.error("KUBELINT did not find an implementationtype for KUBELINTER implementation")


    # create bundle
    logging.info("Creating Bundle of all Policies")
    create_stackrox_bundle(config=mirror_config)
    logging.info("Creating Kube-linter file")
    create_kubelinter_file(kl_conf=kubelint_conf)


def fix_bsi_category(content):
    '''helper function to add BSI category'''
    if "categories" in content:
        if "BSI" not in content["categories"]:
            content["categories"].append("BSI")
            logging.info("Auto-added category BSI: %s", content["name"])
    else:
        content["categories"] = ["BSI"]
        logging.info("Auto-added categories section: %s", content["name"])
        logging.info("Auto-added category BSI: %s", content["name"])
    return content

def stackrox_manual(mpol):
    '''consistency checks for manually written policies'''
    # if the implementationType is manual we check, that a file exists with the correct filename
    # generate filename
    filename = POLICY_DIR + mpol['policyID'] + '-' + mpol['filename'].lower() + '.json'
    if os.path.isfile(filename):
        logging.info("file exists: %s", filename)

        with open(filename, 'r', encoding='utf8') as manual_policy:
            policies_content = json.load(manual_policy)

        # We add a category BSI to Bundle all Policies
        for policy in policies_content["policies"]:
            policy = fix_bsi_category(content=policy)

        # we write changes to the file.
        # We do not check if we had any changes, this way we can also unify indentation
        with open(filename, 'w', encoding='utf8') as manual_policy:
            json.dump(policies_content, manual_policy, indent=2)
    else:
        logging.error("file does not exist: %s",filename)

def stackrox_upstream(upol,upol_disabled_state):
    '''this function downloads an upsream policy, overwrites parts as defined by the spec and writes it to disk'''
    with urllib.request.urlopen(upol['stackrox']['src']) as orig_content:
        content = json.load(orig_content)

        # We add a category BSI to Bundle all Policies
        content = fix_bsi_category(content)

        # We overwrite attributes with the mirror values, if specified
        for attribute in ["name", "description", "rationale", "remediation"]:
            if len(upol[attribute]) != 0 and content[attribute]:
                content[attribute] = upol[attribute]

        # We overwrite attributes specific to stackrox
        for rox_attribute in ["id"]:
            if len(upol["stackrox"][rox_attribute]) != 0 and content[rox_attribute]:
                if upol["stackrox"][rox_attribute] == content[rox_attribute]:
                    logging.error("Attribute %s is identical upstream and in policy specification", rox_attribute)
                    exit(1)
                content[rox_attribute] = upol["stackrox"][rox_attribute]

        # We change to nonDefault policy
        content["isDefault"] = False
        if "disabled" in upol["stackrox"]:
            content["disabled"] = upol["stackrox"]["disabled"]
        else:
            content["disabled"] = upol_disabled_state

        # Overwriting, if necessary
        if "overwrite" in upol["stackrox"]:
            if len(upol["stackrox"]["overwrite"]["policySections"]) != 0:
                content["policySections"] = upol["stackrox"]["overwrite"]["policySections"]

        # write policy into policies list, to have a importable format
        policy_dict = {'policies': [content,]}

        # generate filename from name
        outfilename = POLICY_DIR + upol['policyID'] + '-' + upol['filename'].lower() + '.json'

        with open(outfilename, 'w', encoding='utf8') as json_file:
            json.dump(policy_dict, json_file, indent=2)

def create_stackrox_bundle(config):
    '''this function takes the policies from the spec and creates a bundle consisting of all json files'''
    policy_list = []
    bundle_filename = './bundle.json'
    for policy in config['policies']:
        # we cant parse files we dont implement, so we skip them
        if policy["stackrox"]["implementationType"] == "none":
            logging.info("processing: %s as none - SKIPPED", policy["policyID"])
            continue

        # we construct the filename
        file = POLICY_DIR + policy['policyID'] + '-' + policy['filename'] + ".json"
        logging.info('processing: %s',file)
        # check if the file exists, otherwise log error, we should have a file for every implemented policy
        if not os.path.isfile(file):
            logging.error("%s not found", file)
            continue

        # if we have afile, we can open it, extract the policies included and add them to our list
        with open(file, 'r', encoding='utf8') as pol_file:
            pol_file_content = json.load(pol_file)
            # since some of the policies are manually constructed, we check if the field we need exists.
            if "policies" in pol_file_content:
                # for each element in the policies list, we add it to the bundle
                policy_list.extend(pol_file_content['policies'])
            else:
                logging.error("%s has wrong json format", file)

    # create bundle by writing the list to a json file
    bundle= {'policies': policy_list}
    with open(bundle_filename, 'w', encoding='utf8') as bundle_file:
        json.dump(bundle, bundle_file, indent=2)

def add_kubelinter_templates(kl_item):
    '''this function takes the policies from the spec and creates a kubelinter yaml'''
    templates = []
    for index, template in enumerate(kl_item["kubelinter"]["templates"]):
        # create template based on the policy-spec
        templateconf = template
        # unify metadata
        # we use a index since we may need more than one kubelinter rule to implement a policy
        # this is different to how we implement stackrox policies, which are distinct a level up.
        # imho it is a better approach to iterate at the implementation level and
        # not have a policyID multiple times
        templateconf = {}
        templateconf["name"] = kl_item["policyID"] + "-" + kl_item["filename"] + "-" + str(index)
        templateconf["remediation"] = kl_item["remediation"]
        templateconf.update(template)
        # to ensure, we have a consistency as the first item in the dict, we pop it to front
        # templateconf = {"name": templateconf.pop("name"), **templateconf}
        # add template to kubelintconf list
        templates.append(templateconf)
    return templates

def create_kubelinter_file(kl_conf):
    kubelint_filename = '.kube-linter.yaml'
    if os.path.exists(kubelint_filename):
        os.remove(kubelint_filename)

    with open(kubelint_filename, 'w') as kubelint_file:
        kubelint_file.write('# autogenerated file\n# customize to your needs\n')
        kubelint_file.write('# pay special attention to\n# - 007\n# - 008\n# - 009\n# - 026\n')
        yaml.dump(kl_conf, kubelint_file, sort_keys=False)

if __name__ == "__main__":
    main()
